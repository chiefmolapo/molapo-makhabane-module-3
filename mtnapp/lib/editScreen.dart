import 'package:flutter/material.dart';
import 'package:mtnapp/dashbord.dart';

void main() {
  runApp(const EditProfilePage());
}

class EditProfilePage extends StatelessWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        const Text(
          'Edit your account',
          style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
        ),
        TextFormField(
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Name',
              prefixIcon: Icon(Icons.person)),
        ),
        const SizedBox(height: 20),
        TextFormField(
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'surname',
              prefixIcon: Icon(Icons.person)),
        ),
        const SizedBox(height: 20),
        TextFormField(
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'phone number',
              prefixIcon: Icon(Icons.phone)),
        ),
        const SizedBox(height: 20),
        TextFormField(
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'physical adress',
              prefixIcon: Icon(Icons.location_city)),
        ),
        const SizedBox(height: 20),
        const TextField(
          obscureText: true,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'password',
              prefixIcon: Icon(Icons.lock)),
        ),
        const SizedBox(height: 20),
        Container(
            height: 40,
            width: 100,
            decoration: BoxDecoration(
                color: Colors.yellow,
                borderRadius: BorderRadius.circular(100),
                gradient: const LinearGradient(
                    colors: [Colors.amber, Colors.orangeAccent])),
            child: MaterialButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return const DashbordonePage();
                  }),
                );
              },
              child: const Text(
                'Edit',
                style: TextStyle(
                  color: Color.fromARGB(239, 58, 52, 52),
                  fontSize: 25,
                ),
              ),
            )),
      ]),
    );
  }
}
