import 'package:flutter/material.dart';
import 'package:mtnapp/dashbord.dart';

void main() {
  runApp(const LoginPage());
}

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: const Text('Login Screen'),
            backgroundColor: Color.fromARGB(248, 240, 206, 15),
            foregroundColor: Colors.black),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              'Hello',
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
            ),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'email',
                  prefixIcon: Icon(Icons.email)),
            ),
            const SizedBox(height: 20),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'password',
                  prefixIcon: Icon(Icons.lock)),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                TextButton(
                    onPressed: null,
                    child: Text(
                      'Forgot password?',
                      style: TextStyle(
                          fontSize: 15,
                          color: Color.fromARGB(255, 148, 148, 52)),
                    ))
              ],
            ),
            const SizedBox(height: 20),
            Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(100),
                    gradient: const LinearGradient(
                        colors: [Colors.amber, Colors.orangeAccent])),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const DashbordonePage();
                      }),
                    );
                  },
                  child: const Text(
                    'Login',
                    style: TextStyle(
                      color: Color.fromARGB(239, 58, 52, 52),
                      fontSize: 25,
                    ),
                  ),
                )),
            const SizedBox(height: 20),
            const Divider(
              height: 10,
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Dont have an acount?'),
                TextButton(
                    onPressed: null,
                    child: Text(
                      'Register an account?',
                      style: TextStyle(
                          fontSize: 15,
                          color: Color.fromARGB(255, 148, 148, 52)),
                    ))
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
