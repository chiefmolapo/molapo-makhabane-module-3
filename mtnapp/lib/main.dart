import 'package:flutter/material.dart';
import './loginScreen.dart';
//import './registerScreen.dart';
//import './editScreen.dart';
//import './dashbord.dart';
import './floatingdashboard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const LoginPage();
  }
}
