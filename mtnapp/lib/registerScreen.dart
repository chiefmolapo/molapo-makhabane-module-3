import 'package:flutter/material.dart';
import 'package:mtnapp/loginScreen.dart';

void main() {
  runApp(const RegisterPage());
}

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color.fromARGB(248, 240, 206, 15),
          foregroundColor: Colors.black,
          title: const Text('Register Screen '),
        ),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              'Hello Register your account',
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
            ),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Name',
                  prefixIcon: Icon(Icons.person)),
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'surname',
                  prefixIcon: Icon(Icons.person)),
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'email',
                  prefixIcon: Icon(Icons.email)),
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'phone number',
                  prefixIcon: Icon(Icons.phone)),
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'physical adress',
                  prefixIcon: Icon(Icons.location_city)),
            ),
            const SizedBox(height: 20),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'password',
                  prefixIcon: Icon(Icons.lock)),
            ),
            const SizedBox(height: 20),
            Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(100),
                    gradient: const LinearGradient(
                        colors: [Colors.amber, Colors.orangeAccent])),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const LoginPage();
                      }),
                    );
                  },
                  child: const Text(
                    'Register',
                    style: TextStyle(
                      color: Color.fromARGB(239, 58, 52, 52),
                      fontSize: 25,
                    ),
                  ),
                )),
            const SizedBox(height: 20),
            Divider(
              height: 10,
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Have an acount?'),
                TextButton(
                    onPressed: null,
                    child: Text(
                      'Login',
                      style: TextStyle(
                          fontSize: 15,
                          color: Color.fromARGB(255, 148, 148, 52)),
                    ))
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
