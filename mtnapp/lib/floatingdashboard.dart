import 'package:flutter/material.dart';
import './dashbord.dart';
import './loginScreen.dart';
import './editScreen.dart';

void main() {
  runApp(DadhboardFloating());
}

class DadhboardFloating extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    //TODO: implement createState
    return DadhboardFloatingState();
  }
}

class DadhboardFloatingState extends State<DadhboardFloating> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
              centerTitle: true,
              title: const Text('Floating buttons'),
              backgroundColor: const Color.fromARGB(248, 240, 206, 15),
              foregroundColor: Colors.black),
          body: Padding(
            padding: const EdgeInsets.all(30),
            child: Column(children: [
              const Text(
                'Menu',
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        margin: const EdgeInsets.symmetric(horizontal: 25),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Stack(children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child:
                                        //const Icon(Icons.laptop)
                                        Image.asset('./assets/images/book.jpeg',
                                            width: 120)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text(
                                        'this is one of the best Book to read its a dart pdf book',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 1.5,
                                        )),
                                    const Text('Learn how to program pdf',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 1.5,
                                        )),
                                    const Text('R1700',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          height: 1.5,
                                          color:
                                              Color.fromARGB(255, 194, 175, 2),
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                          const Positioned(
                              top: 0,
                              right: 0,
                              child: Icon(
                                Icons.favorite,
                                size: 25,
                              ))
                        ]),
                      )
                    ]),
              ),
              const SizedBox(
                height: 20,
              ),
            ]),
          ),
          //SizedBox(height: 40),
          // bottomNavigationBar: BottomAppBar(color: Colors.yellow,

          // child:Container(height: 50 ,) ,),
          floatingActionButton: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                    backgroundColor: Colors.yellow,
                    child: const Icon(
                      Icons.home,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return const DashbordonePage();
                        }),
                      );
                    }),
                const SizedBox(
                  width: 5,
                ),
                FloatingActionButton(
                    backgroundColor: Colors.yellow,
                    child: Icon(
                      Icons.person_add,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return const EditProfilePage();
                        }),
                      );
                    }),
                const SizedBox(
                  width: 5,
                ),
                FloatingActionButton(
                  backgroundColor: Colors.yellow,
                  child: const Icon(
                    Icons.exit_to_app,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const LoginPage();
                      }),
                    );
                  },
                ),
              ],
            ),
          )),
    );
  }
}
