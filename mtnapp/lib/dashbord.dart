import 'package:flutter/material.dart';

void main() {
  runApp(const DashbordonePage());
}

class DashbordonePage extends StatelessWidget {
  const DashbordonePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Color.fromARGB(248, 240, 206, 15),
              foregroundColor: Colors.black,
              title: Text('DashBoard Menu'),
            ),
            body: Padding(
              padding: const EdgeInsets.all(30),
              child: Column(children: [
                const Text(
                  'Menu',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          margin: EdgeInsets.symmetric(horizontal: 25),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Stack(children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(15),
                                      child:
                                          //const Icon(Icons.laptop)
                                          Image.asset(
                                              './assets/images/book.jpeg',
                                              width: 120)),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                          'this is one of the best iphone with high performace',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            height: 1.5,
                                          )),
                                      const Text('Iphone 13 pro',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            height: 1.5,
                                          )),
                                      const Text('R17 000',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            height: 1.5,
                                            color: Color.fromARGB(
                                                255, 194, 175, 2),
                                          )),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            const Positioned(
                                top: 0,
                                right: 0,
                                child: Icon(
                                  Icons.favorite,
                                  size: 25,
                                ))
                          ]),
                        )
                      ]),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                    height: 40,
                    width: 400,
                    decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.circular(100),
                        gradient: const LinearGradient(
                            colors: [Colors.amber, Colors.orangeAccent])),
                    child: MaterialButton(
                      onPressed: () {},
                      child: const Text(
                        'DashBoard',
                        style: TextStyle(
                          color: Color.fromARGB(239, 58, 52, 52),
                          fontSize: 25,
                        ),
                      ),
                    )),
                const SizedBox(
                  height: 20,
                ),
                Container(
                    height: 40,
                    width: 400,
                    decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.circular(100),
                        gradient: const LinearGradient(
                            colors: [Colors.amber, Colors.orangeAccent])),
                    child: MaterialButton(
                      onPressed: () {},
                      child: const Text(
                        'Logout',
                        style: TextStyle(
                          color: Color.fromARGB(239, 58, 52, 52),
                          fontSize: 25,
                        ),
                      ),
                    )),
              ]),
            )));
  }
}
